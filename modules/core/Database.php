<?php
/**
 * Core Database class
 * @copyright Blioxxx
 * @package core
 */

namespace Blioxxx\Hotlinks\Core;


class Database
{
    private $dbhost;
    private $dbdatabase;
    private $dbuser;
    private $dbpass;

    // Get the currect db settings
    public function __construct()
    {
        $string = file_get_contents('../../config/local.json');
        $json_d = json_decode($string);

        $this->dbhost = $json_d['dbhost'];
        $this->dbdatabase = $json_d['dbdatabase'];
        $this->dbuser = $json_d['dbuser'];
        $this->dbpass = $json_d['dbpass'];
    }

    public function getHost()
    {
        return $this->getHost();
    }
}